const code = require('../config/http-code');

module.exports.validateId = (req, res, next) => {
    if (isNaN(req.params.id))
        return res.sendStatus(code.BAD_REQUEST);
    else if (parseInt(req.params.id) < 0)
        return res.sendStatus(code.BAD_REQUEST);
    next()
};

module.exports.validateSearchSample = (req, res, next) => {
    if (req.params.sample.length === 0)
        return res.sendStatus(code.BAD_REQUEST);
    else if (req.params.sample.length > 500)
        return res.sendStatus(code.BAD_REQUEST);
    next()
};

module.exports.validatePost = (req, res, next) => {
    req.checkBody('title', 'Invalid title').notEmpty();
    req.checkBody('authorId', 'Invalid author').notEmpty();
    req.checkBody('imageURL', 'Invalid text image url').notEmpty();
    req.checkBody('text', 'Invalid text').notEmpty();
    req.checkBody('createdAt', 'Invalid create date').notEmpty();
    req.checkBody('updatedAt', 'Invalid update date').notEmpty();

    const errors = req.validationErrors();
    if (!errors) return next();

    let response = {errors: []};
    errors.forEach((err) => {
        response.errors.push(err.msg);
    });
    res.statusCode = code.BAD_REQUEST;
    return res.json(response);
};

