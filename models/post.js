const {pool} = require('../config/postgres');

module.exports.findAll = async () => {
    const query = 'SELECT * FROM post;';
    const client = await pool.connect();

    try {
        const res = await client.query(query);
        return res.rows
    } catch (e) {
        console.log(e.stack)
    } finally {
        client.release();
    }
};

module.exports.findById = async (id) => {
    const query = 'SELECT * FROM post WHERE id = $1;';
    const client = await pool.connect();

    try {
        const res = await client.query(query, [id]);
        return res.rows[0];
    } catch (e) {
        console.log(e.stack)
    } finally {
        client.release();
    }
};

module.exports.findBySample = async (sample) => {
    // TODO: fix it
    const query = `SELECT * FROM post WHERE (title || text) ILIKE '%${sample}%';`;
    const client = await pool.connect();

    try {
        const res = await client.query(query);
        return res.rows;
    } catch (e) {
        console.log(e.stack);
    } finally {
        client.release();
    }
};

module.exports.save = async (dataset) => {
    const query = `INSERT INTO post(author_id, title, image_url, text, created_at, updated_at) 
                        VALUES($1, $2, $3, $4, $5, $6);`;
    const client = await pool.connect();

    try {
        await client.query(query, dataset);
    } catch (e) {
        console.log(e.stack);
    } finally {
        client.release();
    }
};

module.exports.update = async (dataset) => {
    const query = `UPDATE post 
                        SET author_id = $1, title = $2, image_url = $3, text = $4, created_at = $5, updated_at = $6 
                        WHERE id = $7`;
    const client = await pool.connect();

    try {
        await client.query(query, dataset);
    } catch (e) {
        console.log(e.stack);
    } finally {
        client.release();
    }
};

module.exports.delete = async (post) => {
    const query = 'DELETE post WHERE id = $1';
    const client = await pool.connect();

    try {
        await client.query(query, [post.id]);
    } catch (e) {
        console.log(e.stack);
    } finally {
        client.release();
    }
};

module.exports.deleteById = async (postId) => {
    const query = 'DELETE FROM post WHERE id = $1';
    const client = await pool.connect();

    try {
        await client.query(query, [postId]);
    } catch (e) {
        console.log(e.stack);
    } finally {
        client.release();
    }
};
