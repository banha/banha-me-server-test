const {pool} = require('../config/postgres');

module.exports.findAll = async () => {
    const query = 'SELECT * FROM user_table;';
    const client = await pool.connect();

    try {
        const res = await client.query(query);
        return res.rows;
    } catch (e) {
        console.log(e.stack)
    } finally {
        client.release();
    }
};

module.exports.findByUsername = async (username) => {
    const query = 'SELECT * FROM user_table WHERE email = $1';
    const client = await pool.connect();

    try {
        const res = await client.query(query, [username]);
        return res.rows[0];
    } catch (e) {
        console.log(e.stack)
    } finally {
        client.release();
    }
};

module.exports.findById = async (userId) => {
    const query = 'SELECT * FROM user_table WHERE id = $1';
    const client = await pool.connect();

    try {
        const res = await client.query(query, [userId]);
        return res.rows[0];
    } catch (e) {
        console.log(e.stack)
    } finally {
        client.release();
    }
};
