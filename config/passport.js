const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../models/user');

passport.use(new LocalStrategy(async (username, password, done) => {
        let user = await User.findByUsername(username);
        if (!user) return done(null, false, {error: 'Username is invalid'});

        let res = await bcrypt.compare(password, user.password);

        if (res) return done(null, {id: user.id, username: user.username});
        else return done(null, false, {error: 'Password is invalid'})
    })
);

passport.serializeUser(async (user, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
    let user = await User.findById(id);
    if (!user) return done(user);
    done(null, user);
});

module.exports = passport;
