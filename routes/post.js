'use strict';

const express = require('express');
const router = express.Router();
const Post = require('../models/post');
const code = require('../config/http-code');
const validator = require('../middleware/validator');

router.get('/', getPosts, (req, res) => {
    res.json(req.posts);
});

router.get('/:id', validator.validateId, getPost, (req, res) => {
    res.json(req.post);
});

router.get('/search/:sample', validator.validateSearchSample, findPost, (req, res) => {
    res.json(req.post);
});

router.post('/', checkAuth, validator.validatePost, createPost, (req, res) => {
    res.sendStatus(code.CREATED);
});

router.put('/:id', checkAuth, validator.validateId, validator.validatePost, updatePost, (req, res) => {
    res.sendStatus(code.OK);
});

router.delete('/:id', checkAuth, validator.validateId, deletePost, (req, res) => {
    res.sendStatus(code.OK);
});

function checkAuth(req, res, next) {
    if (req.user) next();
    else res.sendStatus(code.UNAUTHORIZED)
}

async function getPosts(req, res, next) {
    const posts = await Post.findAll();

    if (!posts) return res.sendStatus(code.NOT_FOUND);

    req.posts = posts;
    res.statusCode = code.OK;
    next()
}

async function getPost(req, res, next) {
    const post = await Post.findById(parseInt(req.params.id));

    if (!post) return res.sendStatus(code.NOT_FOUND);

    req.post = post;
    res.statusCode = code.OK;
    next()
}

async function findPost(req, res, next) {
    const post = await Post.findBySample(req.params.sample);

    if (!post) return res.sendStatus(code.NOT_FOUND);

    req.post = post;
    res.statusCode = code.OK;
    next()
}

function createPost(req, res, next) {
    Post.save([
        req.body.authorId,
        req.body.title,
        req.body.imageURL,
        req.body.text,
        req.body.createdAt,
        req.body.updatedAt
    ]);
    next()
}

function updatePost(req, res, next) {
    const urlId = parseInt(req.params.id);
    const postId = parseInt(req.body.id);

    if (urlId !== postId) return res.sendStatus(code.BAD_REQUEST);

    Post.update([
        req.body.authorId,
        req.body.title,
        req.body.imageURL,
        req.body.text,
        req.body.createdAt,
        req.body.updatedAt,
        req.body.id
    ]);

    next()
}

function deletePost(req, res, next) {
    const id = parseInt(req.params.id);
    Post.deleteById(id);
    next();
}

module.exports = router;
