const express = require('express');
const router = express.Router();
const passport = require('passport');
const code = require('../config/http-code');


router.post('/login', passport.authenticate('local'), (req, res) => {
    res.json(req.user);
});

module.exports = router;
